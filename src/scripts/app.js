window.Reveal = require('reveal.js/js/reveal.js');

window.Reveal.initialize({
  controls: false,
  progress: true,
  history: true,
  center: true,
  // default/cube/page/concave/zoom/linear/fade/none
  transition: 'linear',
});
