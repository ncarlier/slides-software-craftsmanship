# Software Craftsmanship.
#
# VERSION 0.0.1

FROM ncarlier/nodejs

MAINTAINER Nicolas Carlier <https://github.com/ncarlier>

# Port
EXPOSE 3000

# Add files
ADD . /opt/slides-software-craftsmanship
WORKDIR /opt/slides-software-craftsmanship
RUN chown node.node -R /opt/slides-software-craftsmanship

# Def. user
USER node
ENV HOME /home/node

# Install App
RUN npm install

ENTRYPOINT ["/usr/bin/npm"]

CMD ["start"]
