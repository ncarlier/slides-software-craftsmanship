var p           = require('./package.json'),
    gulp        = require('gulp'),
    ejs         = require('gulp-ejs'),
    less        = require('gulp-less'),
    gulpif      = require('gulp-if'),
    uglify      = require('gulp-uglify'),
    deploy      = require('gulp-gh-pages'),
    browserSync = require('browser-sync'),
    http        = require('http'),
    st          = require('st'),
    browserify  = require('browserify'),
    transform   = require('vinyl-transform'),
    runSequence = require('run-sequence'),
    del         = require('del');

var NpmImportPlugin = require("less-plugin-npm-import"),
  lessOptions = { plugins: [new NpmImportPlugin()] };

var reload = browserSync.reload;

gulp.task('templates', function () {
  var context = p;
  gulp.src('src/templates/index.html')
  .pipe(ejs(context))
  .pipe(gulp.dest('dist/'));
})

gulp.task('styles', function() {
  gulp.src('src/styles/index.less')
    .pipe(less(lessOptions))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('scripts', function() {
  var browserified = transform(function(filename) {
    var b = browserify(filename);
    return b.bundle();
  });

  return gulp.src(['src/scripts/*.js'])
  .pipe(browserified)
  .pipe(gulpif(process.env.NODE_ENV === 'production', uglify()))
  .pipe(gulp.dest('dist/js'));
});

gulp.task('assets', function() {
  gulp.src('src/images/**').pipe(gulp.dest('dist/images'));
  gulp.src('src/videos/**').pipe(gulp.dest('dist/videos'));
  gulp.src('node_modules/reveal.js/plugin/**').pipe(gulp.dest('dist/js/plugin'));
  gulp.src('node_modules/font-awesome/fonts/**').pipe(gulp.dest('dist/fonts'));
});

gulp.task('watch', function() {
  // Watch template files
  gulp.watch('src/templates/**/*', ['templates', reload]);
  // Watch style files
  gulp.watch('src/styles/**/*.less', ['styles', reload]);
  // Watch javascript files
  gulp.watch('src/scripts/**/*.js', ['scripts', reload]);
  // Watch assets files
  gulp.watch('src/images/**/*', ['assets', reload]);
});

if (process.env.OPENSHIFT_NODEJS_IP) {
  var port = parseInt(process.env.OPENSHIFT_NODEJS_PORT || 3000, 10),
      host = process.env.OPENSHIFT_NODEJS_IP || null;
  gulp.task('serve', ['default'], function(done) {
    http.createServer(
      st({ path: __dirname + '/dist', index: 'index.html', cache: false })
    ).listen(port, host, done);
  });
} else {
  gulp.task('serve', ['default', 'watch'], function() {
    browserSync({
      server: {
        baseDir: "./dist"
      }
    });
  });
}

gulp.task('publish', function () {
  return gulp.src('./dist/**/*').pipe(deploy());
});

gulp.task('clean', del.bind(null, ['dist']));

gulp.task('default', ['clean'], function(cb) {
  runSequence(['templates', 'styles', 'scripts', 'assets'], cb);
});
