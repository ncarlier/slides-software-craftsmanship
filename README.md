# Software Craftsmanship
(l'artisanat logiciel)

## Teaser
Let's do a very quick survey before we start. Who is a developer? Who is proud to be a developer?
Ok. I am sorry to tell you that, but you **suck**!
There is no more value doing such a job. Nowadays, **Software Development** is not a core value, it's just a **commodity**! And like any commodity it can be **bought**.
Code is a technical, minor stuff and it is **secondary**. You have to **delegate** or **outsource** it to subalterns in order to focus on the real valuable things.
The true value of a Software is the **Process**!
Process, planification, organization, standardization, control, measurability and dashboards are the keys for a good... sorry for a **profitable Software**!

Because **short-term profit** is the only thing that matters in a company.

Think different. Think P2P: **Process to Profit**

This is what Software Development has to be.

Thanks.

If you embrace this new vision of our job, please, we beg you, come to our presentation!!!
And if you don't feel comfortable with this... you are also welcome to come and argue with us.




## About us

[Photos]

Let's introduce ourselves,

* I am Nicolas...
* I am Benjamin, I spent 8 years in the former TUM business unit on various project for Orange : spam fighting, mobile apps back-end, AppShop admin back-end, OCC (Cloud for individuals) ... and since last year I belong to SDCO "Application Server and Middleware" team.


In 45 minutes, we will try together to make you an overview about what is Software Craftsmanship.

## What is craftsmanship ?

First, let's do some semantic and try to define what Craftsmanship is, and is not.

### Examples from real life...

Here a couple of illustrations to help the definition...

#### Clothes

Let's begin by a funny one.

![tailor-made suit][./src/images/what_is_craftsmanship/tailor_made_suit.jpg]

VS

![ready to wear suit][./src/images/what_is_craftsmanship/ready_to_wear_suit.jpg]

Ok, a such comparisons is a bit overkill, but. Both are wearing a suit. After all, the functional requirement is met. It's just a clothes.
But in one case the costume was made to fit the measurements of the person. And this made the whole difference.
It's not just the matter of what is done but how it is done.

#### Buildings

Let's continue by something more serious

![photo atrium][./src/images/what_is_craftsmanship/atrium.jpg]

![photo atrium][./src/images/what_is_craftsmanship/atrium2.jpg]

"L'Atrium" is the office building where Worldline Lyons employees used to work from 2006 to 2014. It was built in 2005.
Outside point of view: A modern building which impresses your clients at first view !
Inside point of view, during exploitation:

* at least...
    * 4 floods at different places of the building
    * 2 fire starts
    * 2 windows that fell off from the building to the street ! => expensive security operations during several months
    * dysfunctional toilets, dysfunctional parking doors, temperature problems in the offices, mice in the corridors...

* and a lot of associated disturbances:
    * many proceedings to manage and fix those problems (with the insurance companies, building companies, etc etc),
    * drown cars, drown offices,
    * building evacuations during working hours,
    * scaffolds around the building during several months, uncomfortable temperatures, etc

OK, hundreds of employees work there every day. But still... is this a well-crafted building ?
Can the architect and the company that built this be entirely proud of their work ? 
Assuming that the building was delivered on time with no unplanned costs, and matched the requirements...
Was this building fully made in a professional way ? Would you ask the architect to build your own house ?

No, a really professional behaviour would not have lead to deliver a building with so many major hidden defaults and construction faults.
It's not only about respecting a contract, but also to put professionalism and pride in your work.

### A look at the dictionary

Let's go back more formal. What is the definition of Craftsmanship.

The dictionary told us:

> Skill in a particular craft

Ok what is a craft:

> An activity involving skill in making things (by hand)

It seems to be only a matter of skill. Go on and try to be more specific.

What about _Software_ Craftsmanship ?

Wikipedia tells us:

> It is an approach to software development that emphasizes the coding skills of the software developers themselves.

So it's just a matter of skill? No! Don't be abused by a too quick reading. A far better definition comes from the manifesto.


### The Software Craftsmanship manifesto

The manifesto was published in 2009. It was the result of several workshops with
people from various developing movements (Agile, XP ...) having the feeling that
the Agile Manifesto was not enough.
Agile had changed the way of developing software, but it did not prevent companies
from producing poor-quality software, constantly and industrially.
All those people agreed on the assessment that Agile introduced many processes that
became, over time, more important than technical practices.

Don't forget that Agile methodology did not solve problems but shows them faster.

> People that don't understand core values focus on processes
Rui Carvalho

The manifesto comes to value:

![manifesto][./src/images/the_software_craftsmanship_manifesto/manifesto.png]

* Not only working software, but also well-crafted software
* Not only responding to change, but also steadily adding value
* Not only individuals and interactions, but also a community of professionals
* Not only customer collaboration, but also productive partnerships

It's subtitled by "Raising the bar" !

Since then, the Software Craftsmanship movement was born and made a lot of buzz.
Many publications were made, many meetups, many conferences and many books.

<!-- TODO: put some illustrations. -->
http://software-craftsmanship.meetup.com/


Naturally there is partisans and detractors.

As for many other manifesto, the sentences can be interpreted. This can be a reason of detraction.
Together we will focus on each quote and try to explain what is our interpretation.


## Well-crafted software

> Not only working software, but also well-crafted software


To explain what a well-crafted software is, first we have to simply define what a software is.
It's a bunch of code that implements functional and non-functional requirements.

The functional requirements define WHAT the software is supposed to do.
The non-functional requirements define HOW the software is supposed to do.

The "functional requirements" term seems to be obvious and self-explanatory.

The "non-functional requirements" term embraces all the rest, and thus needs a short overview.

It's about:

- Testability: the ability to be tested. Think KISS, TDD, BDD.
- Performance: the ability of doing the job quickly
- Scalability: the ability of doing the job in time regardless the load
- Availability: the ability of doing the job regardless disturbances
- Security: the ability to protect sensitive data
- Accessibility: the ability to give the same level of service regardless the user capacities
- Monitoring: the ability to see what is going on
- Management: the ability to operate the system
- Audit: the ability to measure the behavior
- Flexibility: the ability to adapt (aka clean code: a software that you are not scared to touch)
- Extendability: the ability to evolve
- Legal, Regulatory and Compliance: the ability to do things according the law
- Internationalization: the ability to handle different languages
- Localization: the ability to handle different cultures

All of these requirements cannot be handled the same way, at the same level of implication and quality.
Quality is not perfection, but pragmatic attitude. You will have to find compromise solutions.
You have to put weight on each of them and make a priority list.
It's exactly the same for functional requirements.

Note that non-functional requirements can influence functional requirements and vice versa.
For instance:
If performance matter, build a complex search form with tons of fields can be an issue.
If database requires relationship and strong consistency, scalability can be an issue.

A well-crafted software takes care of functional and nonfunctional requirements the same way.
It's a balance of WHAT and HOW.
The tricky things is to keep the balance right according constraints.

Constraints are numerous and for some well known:

- Time and budget constraints (the famous one!)
- Technologies constraints:
  - Approved technology lists (Of the company: java, T5, fw3 ;) (Or of the client: Weblogic, Drupal, etc.)
    But think about that: "if the only tool you have is a hammer, everything looks like a nail"
  - Existing systems and interoperability (service tools, watchdog)
  - Target deployment platform (centos)
  - Technology maturity (and the support of TO)
  - Vendor relationship
  - Past failures (Some projects fails using Hazelcast, thereby Hazelcast is not a good product...)
  - ...
- People constraints (For this strategic project you will work with 3 trainees and 2 externals)
- Organizational constraints (purchasing department, ISMP, ...)

Constraints can not be avoided but they can also be prioritized.

That is our definition of a well-crafted software.

Just remember:

> How it is done is as important as having it done




## Steadily adding value

> Not only responding to change, but also steadily adding value

### Writing lines is not enough

"Steadily adding value" is not about adding new features or fixing bugs. This quote means that implementing backlog items is not enough:
Adding a lot of code in a lot of classes instead of factorising is quite easy, making the code hard to understand and unmaintainable is really easy.
"How to write unmaintainable code" : https://www.thc.org/root/phun/unmaintain.html

> Many (Agile) projects are now, steadily and iteratively, producing crap mediocre software

A growing project has a natural tendency to become unstructured, harder to test and to maintain.
We are particularly concerned in Worldline, since we have mainly middle-sized and big projects.
Being a software engineer means more than writing code, it means that we have to deal with that problematic.

Refactoring should not be seen as an issue. When you add a new feature you have to add this new feature for the best!
And refactoring is a tool to help you in this way.

### The boy scout rule

"Steadily adding value" is about the boy scout rule : "do not degrade your project". If the developers apply it, then they keep the project in a good state.

Allegory of a software developed entirely by unsupervised young trainees 
We know that phenomenon happens, and it's our responsibility as developers to anticipate that there will be evolutions during several years,
to prepare the next years for our company, and not to weight the technical debt.

Ex : on your big and rather old project, think of your project manager that pains to explain to the client, why this cost estimation is so high for this feature ?
Because it's so expensive that we have to decommission half of the planned features.
The true reason is : __technical debt__!
The initial feature was developed poorly and we don't exactly know how it works, and moreover we have to retest a lot of things manually after changing
it because there is no test automation.
And of course the project manager has to lie, to explain it a more "politically correct" way. That should not happen !

It's all about keeping the application well structured, keeping the code readable and clean, easy to test (and tested).

### Steadily adding value is a win-win

This is obvious, but this is often forgotten in our everyday work. The boy scout rule is underrated because we are always focusing on short-term.
On a lot of projects, quality is the first thing that we sacrifice: everybody knows projects on which the unit tests are removed from
the planning before the beginning of the developments...

So, keep on reminding to your co-workers this obvious advantages of a maintainable project:

* For the developer, it means that he will keep the project and make his own environment better.
* For the project manager, it means that the velocity will remain good on the long term, and so the ROI won't drop.
* For the client, it means the product will be easy to make evolve to fit his business needs.


Transition : this point shows that we are a quite immature industry, that's why we have to improve our profession,
not only individually, but as a community of professionals like right now in this place :)


## Community of professionals

> Not only individuals and interactions, but also a community of professionals

### Apprenticeship and Mentoring


Software Craftsmanship is not a level, but a culture. And a true craftsman has to __know__ and to __transmit__ it.

#### Knowing practices

Especially in your job, knowledge is something you have to feed all your career.
And the better way to acquire knowledge is practice.

And practice can be made in many funny way:

TODO: Explain the items

- Learn
- Dojos
- Code retreat
- Pair programming / extreme programming
- Code revue (collective, pair)
- Continuous improvement

Code retreat is actually a day-long workshop focused on the fundamentals of software
development and design, providing developers the opportunity to take part in focused
exercises away from the daily routine of "writing useable code."

We have to train on those practices. Because every time we try something new, our productivity first drops during
a given amount of time before becoming better than initially.
That's normal, that's why we have to master those practices so that they become natural.

> Craftsmanship is a long journey to mastery

#### Sharing and transmitting

> With Great Knowledges Comes Great Responsibility

In France, in a lot of craft professions, there is this old tradition called the "companionship". You probably have heard of it.
Companionship is a network of craftsmen doing the same craft, where knowledge, know-hows and identities are transmitted.
Just as software craftsmanship, companionship is not an honorary title or a diploma, but a professional and philosophical state of mind.
For example, the companions had (and still have) to do a "Tour de France" where they do many internships in many workshops all
around the country, and then at the end of their education they have to do a particular work called their masterpiece.

Think about that, and try to make your own "Tour de France" by:

* Talking with colleagues from other BUs / entities
* Writing a blog
* Sharing on BlueKiwi
* Doing presentations (if possible about software craftsmanship ;-))
* Participate to externals meetings like User Groups, Meetups, Conferences, etc.

<!-- a revoir enssemble -->
We must prepare the future of our profession(s): firstly for us because (almost) everyone in this room will still be working
in software development in ten years, and also for the next generations of developers.

Our profession is currently rather discredited, we are often not considered as well as other engineers (in France)
that have the same graduation level. For example, our average salaries are lower, with the same experience.
We must still prove, as a corporation, that developers are highly professional.
<!-- / a revoir enssemble -->

### Professionalism

Mancuso says: it's the word that sums up the best the "software craftsmanship".
In fact almost every important quote in this presentation (choose the right tool, do not degrade your product)
applies this human behaviour.

Professionalism is not in practices (like "do beautiful code" or "do TDD"). It's also not "doing the right thing" (=effectiveness),
which is the rather the agile thing.
Professionalism is: doing the things right. i.e. do not commit or deliver unfinished/poorly written/untested/quick and dirty work!
We want to be proud of the code we produce, we are responsible of what we produce.

### Passion and Love

And maybe the most important aspect. The love of our job.

Software is everywhere in this World and we are people who are buildling this new World.
You can do a software (for example a mobile app) without project leader, but not without a developer.
We should be proud of it!

Pride is not vanity. We have to be respectful and humble.
http://blog.codinghorror.com/the-ten-commandments-of-egoless-programming/

Love what you do, with mutual respect. And thanks to that you will find the strength to make this World a better place.
You might think: "I cannot change this by my own". Just you? Maybe not. But read the hummingbird's story and think again. ?

All this define a Community of professionals.




## Productive partnerships

> Not only customer collaboration, but also productive partnerships

### Definition

In Worldline, ten years ago,the working model of the company was to act as partner of our clients, not just as their employees.
The client - provider logic is more a contractual thing, to decide how much we are going to be paid, but it should not define
the way we work together. We wanted to be considered by our clients as partners, and we often succeeded to do it, in those days.

This quote means the same thing: we are not just collaborators / interns / contractors. So, we should not behave as factory
workers who just take orders, we should be as concerned as the stakeholders, we should understand our client's business.

Mancuso says : if your client is not OK with that, leave the company! well that's a little extreme...

### The developer's role is central

Because everybody is hostage of the software, of your ability to make the code of the product evolve fast.
It's particularly true in agile projects: to enable the business to be agile, to allow new ideas to be implemented,
every module/package/piece/... of the code should be agile and well-tested.
As a developer, you have the big responsibility of keeping your product well-crafted.
Nobody can do it for you, and it is part of your role to explain when some choices degrade the product's quality.

Absence of quality have costs:

 - Strategic (client satisfaction)
 - Human (demotivation lead to turnover, turnover leads to risk, risk leads to financial loss)
 - Financial (technical debt : maintenance cost, bugs cost, ...)

 Think back to the building and its floods, fires alarms...

![manifesto][./assets/images/productive_partnerships/cost_of_bugs.jpg]

### So, be involved

Take interest to the other developers, client, project leader... and talk with them.
Don't be "the guy in the office" emerging only to buy cola (cf egoless programming).
The guy in the room is out of touch, out of sight, and out of control and has no place in an open, collaborative environment.

Take part to the requirements.

Have the ability to say no. Because if not it will hurt! But "Don't be evil", be able to say "no" the right way: explain !

### "don't discuss practices, discuss value"

Speak your project leader's language : explain costs and added values of bad and good practices.

How to speak to your project leader/manager ? when he/she wants you ...

* to skip the development of tests
* not to refactor the monolithic class you have to update 
* to abandon TDD because it's time-consuming
* not to do pair programming
* to finish your dev on time, no matter the state of the project

Answers:

* tests : shorten the feedback loop (with automation), forces up to decouple code into smart testable pieces, give constant feedback when developping...
* refactor : keeps the code well organized, increase maintainability, have a steady velocity on the long-term
* TDD : shorten the feedback loop, help organizing your work and decoupling your code, continuous feedback on your on-going work, implicit documentation (cf BDD)
* pair programming : shorten feedback loop, share coding practices and code knowledge
* ...

This is how to build a productive partnerships.


## Conclusion

Software Craftsmanship is not...

- A function or a graduation
- An ivory tower (don't fall in the Dunning Kruger Effect)
- A proselyte church
- A guarantee of success on your project...

... but the lack of it can be the main cause of failure on your project.

It's all about the love of a well done job.

Or at last quoting Sandro Mancuso:

> Software-Craftsmanship is all about putting responsibility, professionalism, pragmatism, pride back into software development.

Thanks.


## Sources

http://manifesto.softwarecraftsmanship.org/#/fr-fr
http://en.wikipedia.org/wiki/Software_craftsmanship
http://fr.slideshare.net/jl.morlhon/software-craftsmanship-en-pratique-agiletour?related=3
http://fr.slideshare.net/JAX_London/agile-day-session-2sandro-mancuso-think-you-dont-need-software-craftsmanship?related=4
http://blog.codinghorror.com/the-ten-commandments-of-egoless-programming/
https://www.parleys.com/author/sandro-mancuso
http://blog.xebia.fr/2011/01/31/software-craftsmanship-en-pratique/
http://blog.octo.com/software-craftsmanship-une-culture-a-transmettre/
http://dannorth.net/2011/01/11/programming-is-not-a-craft/
http://fr.slideshare.net/rhwy/2015-rui-carvalhofeedbackloops16x9v14


